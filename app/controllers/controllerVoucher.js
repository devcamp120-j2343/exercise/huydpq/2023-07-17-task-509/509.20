const { default: mongoose } = require("mongoose");
const voucherModel = require("../models/voucherModel");

// get all voucher
const getAllVoucher = (res, req) => {
    console.log("get all voucher")
    // B1: Thu thập dữ liệu
    voucherModel.find()
        .then((data) => {
            return req.status(200).json({
                message: `Get All Voucher Sucessfully`,
                data
            })

        })
        .catch(err => {
            return req.status(500).json({
                status: "Internal Server Error",
                message: err.message
            })
        })
}
// GET VOUCHER BY ID

const getVoucherById = (res, req) => {
    console.log("Get voucher by id");
    // B1: Thu thập dữ liệu
    let id = res.params.voucherId;
    // B2: Kierm tra
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return req.status(400).json({
            message: "bad request"
        })
    }
    // Xử lý kết quả
    voucherModel.findById(id)
        .then(data => {
            return req.status(200).json({
                message: "Sucessfully",
                data
            })
        })
        .catch(error => {
            return req.status(500).json({
                status: "Internal Server Error",
                message: error.message,
            })
        })
}

// create voucher
const createVoucher = (res, req) => {
    console.log("Create voucher")
    // B1: Thu thập dữ liệu
    let body = res.body
    // B2: Kiểm tra dữ liệu
    if (!body.maVoucher) {
        return req.status(400).json({
            message: "maNuocUong is required",
        })
    }
    if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return req.status(400).json({
            message: "phanTramGiamGia is invalid",
        })
    }
    // B3: Xử lý kết quẩ
    let newVocuher = {
        _id: new mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }

    voucherModel.create(newVocuher)
        .then(data => {
            return req.status(200).json({
                message: `Create vouher sucessfully`,
                data
            })
        })
        .catch(error => {
            return req.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
// update Voucher 

const updateVoucher = (res, req) => {
    console.log("Update Voucher");
    // B1: Thu thập dữ liệu
    let id = res.params.voucherId;
    let body = res.body;
    // Kiểm tra thông tin
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return req.status(400).json({
            message: "bad request"
        })
    };
    if (!body.maVoucher) {
        return req.status(400).json({
            message: "maNuocUong is required",
        })
    };
    if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return req.status(400).json({
            message: "phanTramGiamGia is invalid",
        })
    };
    // Xử lý kêt quả

    let updateVoucher = {
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu,
    }
    voucherModel.findByIdAndUpdate (id, updateVoucher)
    .then(data => {
        return req.status(200).json({
            message: `Update Voucher Sucessfully`,
            data
        })
    })
    .catch(error => {
        return req.status(500).json({
            status: "Internal Server Error",
                message: error.message
        })
    })

}
// delete Voucher 
const deleteVoucher = (res, req ) => {
    console.log(`Delete Voucher `)
    // B1: Thu thập dữ liệu
    let id = res.params.voucherId;
    // Kiểm tra thông tin
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return req.status(400).json({
            message: "bad request"
        })
    };
    // B3: Xử lý kết quả
    voucherModel.findByIdAndDelete(id)
    .then(data => {
        return req.status(200).json({
            message: "Delete voucher Sucessfully",
            data
        })
    })
    .catch(err => {
        return req.status(500).json({
            message: err.message
        })
    })



}

module.exports = { getAllVoucher, createVoucher, getVoucherById, updateVoucher, deleteVoucher }