const express = require("express");
const { getAllDrink, createDrink, getDrinkById, updateDrink, deleteDrink } = require("../controllers/controllerDrink");

const routerDrink = express.Router();

routerDrink.use(express.json());
// get all drink
routerDrink.get("/drink",getAllDrink );

// get drink by id
routerDrink.get("/drink/:drinkId",getDrinkById );

 // Create a Drink
 routerDrink.post("/drink", createDrink)

 // PUT a Drink
 routerDrink.put("/drink/:drinkId",updateDrink)

 // Delete a Drink
 routerDrink.delete("/drink/:drinkId",deleteDrink)

module.exports = {routerDrink}


