// khai báo thư viện express
const express = require("express");

// khai báo middleware

const {
    getAllCoures,
    getCoures,
    postCoures,
    putCoures,
    deleteCoures
} = require("../middlewares/courseMiddleware")

// tạo ra routes
 const router = express.Router();

 router.get("/course", getAllCoures, (req, res) => {
    // console.log("Get all Course")
    res.json({
        message : "Get all Course"
    })
 })

 module.exports = {router};
 