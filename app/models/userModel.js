// khai báo thư viện mongoose
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: Schema.Types.ObjectId,
	fullName:  {
        type: String,
        require: true
    },
	email:  {
        type: String,
        require: true,
        unique: true,
    },
address:  {
    type: String,
    require: true
},
phone:  {
    type: String,
    require: true,
    unique: true,
},
orders: [
    {
        type: Schema.Types.ObjectId,
        ref: "Order",
    }
]

})

module.exports = mongoose.model("User", userSchema)